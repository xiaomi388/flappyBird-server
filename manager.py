# coding:utf-8

from tornado import gen
from utility import pack


class ConnectManager(object):
    def __init__(self):
        self.conns = {}

    def add(self, conn, address):
        self.conns[address] = conn

    def remove(self, address):
        del self.conns[address]

    @gen.coroutine
    def send(self, address, data):
        if address in self.conns:
            yield self.conns[address].send_message(pack(data))

    @gen.coroutine
    def handle(self, address):
        if address in self.conns:
            yield self.conns[address].handle()


manager = ConnectManager()
