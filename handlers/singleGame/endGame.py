# coding:utf-8
import time
from tornado import gen

from handlers.base import BaseHandler


class EndGameHandler(BaseHandler):
    ''' 新建游戏类 '''
    @gen.coroutine
    def handle(self, recv_data, address):
        data = {
            'error': ''
        }
        game_id = recv_data['game_id']
        cur_user = self.get_cur_user(address)
        if cur_user is None:
            data['error'] = u'请重新登录'
            raise gen.Return(data)
        if cur_user not in self.single_games:
            data['error'] = u'该局游戏已结束'
            raise gen.Return(data)
        if self.single_games[cur_user]['game_id'] != game_id:
            data['error'] = u'游戏id错误'
            raise gen.Return(data)
        ttl = recv_data['ttl']
        mode = self.single_games[cur_user]['mode']
        del self.single_games[cur_user]
        pipe_num = recv_data['pipe_num']
        score = recv_data['score']

        c = self.CONN.cursor()
        c.execute(''' INSERT INTO record VALUES
            (?, ?, ?, ?, ?, ?)''',
            (cur_user, game_id, mode, ttl, score, pipe_num))
        self.CONN.commit()
        raise gen.Return(data)
