# coding:utf-8
import time
from tornado import gen

from handlers.base import BaseHandler


class SyncGameHandler(BaseHandler):
    ''' 同步游戏类 '''
    @gen.coroutine
    def handle(self, recv_data, address):
        data = {'error': ''}
        raise gen.Return(data)
