# coding:utf-8
import time
from tornado import gen

from handlers.base import BaseHandler
from utility import to_sha


class NewGameHandler(BaseHandler):
    ''' 新建游戏类 '''
    @gen.coroutine
    def handle(self, recv_data, address):
        data = {
            'error': ''
        }
        cur_user = self.get_cur_user(address)
        if cur_user is None:
            data['error'] = u'session已过期，请重新登录'
            raise gen.Return(data)
        mode = recv_data['mode']
        game_id = to_sha('{0}{1}'.format(time.time(), address))
        self.single_games[cur_user] = {
            'game_id': game_id,
            'mode': mode,
            'start_time': time.time()
        }
        data['content'] = game_id
        print self.single_games
        raise gen.Return(data)
