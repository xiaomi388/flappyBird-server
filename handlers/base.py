# coding:utf-8
import time
from tornado import gen

import sqlite3
from utility import prpcrypt



class BaseHandler(object):
    '''用于处理信息，并返回返回信息的处理类'''

    user_addr = {}
    addr_user = {}
    single_games = {}
    multi_games = {}
    CONN = sqlite3.connect('flappy.db')

    wait_queue = {
        'easy': [],
        'medium': [],
        'hard': []
    }

    @gen.coroutine
    def handle(self, recv_data, address):
        ''' 处理用，需要子类进行重载'''
        pass

    def get_user_addr(self, username):
        ''' 获取某个用户的登陆地址 '''
        if username in self.user_addr:
            return self.user_addr[username]
        return None

    def get_cur_user(self, address):
        '''获取当前用户'''
        if address in self.addr_user:
            return self.addr_user[address]
        return None

    def check_timestamp(self, recv_data):
        ''' 用于检查时间戳是否正确 '''
        now = str(int(time.time()))[:-1]
        pc = prpcrypt('I-LOVE-WY-DADDY!')
        timestamp = recv_data['timestamp']
        timestamp = pc.decrypt(timestamp)
        print now, timestamp
        if abs(int(now) - int(timestamp)) <= 2:
            return True
        return False
