# coding:utf-8
from tornado import gen
from manager import manager

from handlers.base import BaseHandler


class EventHandler(BaseHandler):
    @gen.coroutine
    def handle(self, recv_data, address):
        data = {'error': ''}
        game_id = recv_data['game_id']
        cur_user = self.get_cur_user(address)
        if cur_user is None:
            data['error'] = u'session已过期, 请重新登录'
            return data
        if cur_user not in self.multi_games:
            data['error'] = u'该局游戏已经结束！'
            return data
        if self.multi_games[cur_user]['game_id'] != game_id:
            data['error'] = u'游戏id错误'
            return data
        player_list = self.multi_games[cur_user]['player_list']
        event = recv_data['event']
        if event == 'dead':
            if cur_user in self.multi_games:
                self.multi_games[cur_user]['state'] = 'dead'
        for user in player_list:
            recv_data['method'] = 'recv-event-multi-game'
            if user['player'] != cur_user and user['player'] in self.user_addr:
                address = self.user_addr[user['player']]
                manager.send(address, recv_data)
        raise gen.Return(data)
