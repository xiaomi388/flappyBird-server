# coding:utf-8
from tornado import gen

from handlers.base import BaseHandler


class ExitGameHandler(BaseHandler):
    ''' 新建游戏类 '''

    def clear_queue(self, cur_user, mode):
        for id, user in enumerate(self.wait_queue[mode]):
            if user == cur_user:
                del self.wait_queue[mode][id]

    @gen.coroutine
    def handle(self, recv_data, address):
        data = {'error': ''}
        username = self.addr_user[address]
        self.clear_queue(username, 'easy')
        self.clear_queue(username, 'medium')
        self.clear_queue(username, 'hard')
        if username in self.multi_games:
            del self.multi_games[username]
        raise gen.Return(data)
