# coding:utf-8
import time
from tornado import gen

from handlers.base import BaseHandler
from utility import to_sha
from manager import manager


class NewGameHandler(BaseHandler):
    ''' 新建游戏类 '''

    @gen.coroutine
    def send_wait(self, time, addresses):
        data = {
            'event': 'wait-{}'.format(time),
            'method': 'server-event-multi-game'
        }
        for address in addresses:
            yield manager.send(address, data)
        yield gen.sleep(1)

    @gen.coroutine
    def check_start(self, mode):
        ''' 用于检查多人游戏房间人数是否达到四人，如果是，则开始游戏'''
        if len(self.wait_queue[mode]) < 4:
            raise gen.Return(None)
        addresses = []
        for user in self.wait_queue[mode]:
            addresses.append(self.get_user_addr(user))
        game_id = to_sha('{0}{1}'.format(time.time(), ''.join(str(addresses))))
        player_list = []
        start_time = time.time()
        for id, user in enumerate(self.wait_queue[mode]):
            player_list.append({
                'player': user,
                'player_id': id,
                'score': 0
            })
        seed = int(time.time())
        users = []
        for id, user in enumerate(self.wait_queue[mode]):
            users.append(user)
            self.multi_games[user] = {
                'game_id': game_id,
                'mode': mode,
                'start_time': start_time,
                'player_list': player_list,
                'cur_user_id': id,
                'state': 'live',
                'seed': seed
            }
            address = addresses[id]
            data = self.multi_games[user]
            data['event'] = 'ready'
            data['method'] = 'server-event-multi-game'
            yield manager.send(address, data)
        yield self.send_wait(3, addresses)
        yield self.send_wait(2, addresses)
        yield self.send_wait(1, addresses)
        data = {
            'event': 'start',
            'method': 'server-event-multi-game'
        }
        for address in addresses:
            yield manager.send(address, data)
        self.wait_queue[mode] = []
        # # 给每个客户端每隔10秒确定一下自己的移动频率
        # while True:
        #     move_time = 0
        #     yield gen.sleep(10)
        #     move_time += 200
        #     data = {
        #         'event': 'move-pipe',
        #         'method': 'server-event-multi-game',
        #         'time': move_time
        #     }
        #     flag = 0
        #     for id, user in enumerate(users):
        #         if user in self.multi_games:
        #             manager.send(addresses[id], data)
        #             flag = 1
        #     if flag == 0:
        #         break

    @gen.coroutine
    def handle(self, recv_data, address):
        data = {
            'error': ''
        }
        cur_user = self.get_cur_user(address)
        if cur_user is None:
            data['error'] = u'session已过期，请重新登录'
            raise gen.Return(data)
        mode = recv_data['mode']
        self.wait_queue[mode].append(cur_user)
        yield self.check_start(mode)
        raise gen.Return(data)
