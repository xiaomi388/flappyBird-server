# coding:utf-8

from handlers.base import BaseHandler
from manager import manager

from tornado import gen


class CleanHandler(BaseHandler):
    ''' 清理类，用户用户异常退出'''
    def clear_queue(self, cur_user, mode):
        for id, user in enumerate(self.wait_queue[mode]):
            if user == cur_user:
                del self.wait_queue[mode][id]

    @gen.coroutine
    def handle(self, recv_data, address):
        print '----------------clener---------------------'
        print address
        print self.addr_user
        if address in self.addr_user:
            username = self.addr_user[address]
            # check wait queue
            self.clear_queue(username, 'easy')
            self.clear_queue(username, 'medium')
            self.clear_queue(username, 'hard')
            print 'username:', username
            print 'multi_game:', self.multi_games
            if username in self.multi_games:
                if self.multi_games[username]['state'] == 'live':
                    users = [user['player'] for user in self.multi_games[username]['player_list']]
                    print 'users:', users
                    for user in users:
                        if user != username and user in self.user_addr:
                            _address = self.user_addr[user]
                            print 'address found:', _address
                            data = {
                                'method': 'recv-event-multi-game',
                                'event': 'dead',
                                'user_id': self.multi_games[username]['cur_user_id'],
                                'game_id': self.multi_games[username]['game_id']
                            }
                            manager.send(_address, data)
                del self.multi_games[username]
            del self.addr_user[address]
            if username in self.user_addr:
                del self.user_addr[username]
            if username in self.single_games:
                del self.single_games[username]
