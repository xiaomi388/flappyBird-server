# coding:utf-8

from tornado import gen

from handlers.base import BaseHandler


class LogoutHandler(BaseHandler):
    '''登出处理类'''

    def clear_queue(self, cur_user, mode):
        for id, user in enumerate(self.wait_queue[mode]):
            if user == cur_user:
                del self.wait_queue[mode][id]

    @gen.coroutine
    def handle(self, recv_data, address):
        print 'processing logout'
        data = {'error': ''}
        if address in self.addr_user:
            username = self.addr_user[address]
            del self.addr_user[address]
            if username in self.user_addr:
                del self.user_addr[username]
            if username in self.single_games:
                del self.single_games[username]
            # check wait queue
            self.clear_queue(username, 'easy')
            self.clear_queue(username, 'medium')
            self.clear_queue(username, 'hard')
            if username in self.multi_games:
                del self.multi_games[username]
        raise gen.Return(data)
