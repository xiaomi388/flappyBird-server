#coding:utf-8
from tornado import gen

from handlers.base import BaseHandler
from utility import to_sha


class RegisterHandler(BaseHandler):
    ''' 注册处理类 '''
    @gen.coroutine
    def handle(self, recv_data, address):
        data = {'error': ''}
        username = recv_data['username']
        password = recv_data['password']
        if len(username) < 8:
            data['error'] = u'用户名长度过短'
            raise gen.Return(data)
        elif len(password) < 8 or password.isdigit():
            data['error'] = u'密码太弱'
            raise gen.Return(data)

        password = to_sha(password)
        c = self.CONN.cursor()
        user = c.execute('''SELECT username FROM user 
            WHERE username = ? ''', (username,))
        if len(user.fetchall()) != 0:
            data['error'] = u'该用户名已被占用'
            raise gen.Return(data)
        c.execute('''INSERT INTO user VALUES
            (?,?)''', (username, password))
        self.CONN.commit()
        self.addr_user[address] = username
        self.user_addr[username] = address
        raise gen.Return(data)
