# coding:utf-8

from tornado import gen

from handlers.base import BaseHandler
from utility import to_sha


class LoginHandler(BaseHandler):
    '''登陆处理类'''

    @gen.coroutine
    def handle(self, recv_data, address):
        data = {'error': ''}
        username = recv_data['username']
        password = recv_data['password']

        if username in self.user_addr:
            data['error'] = u'登陆失败，该用户已在别处登陆'
            raise gen.Return(data)

        password = to_sha(password)
        c = self.CONN.cursor()
        user = c.execute(
            '''SELECT username FROM user WHERE
            username = ? and password = ? ''', (username, password))
        num = 0
        for _ in user:
            num += 1
        if num == 0:
            data['error'] = u'用户名或密码错误, 或者用户不存在'
            raise gen.Return(data)
        self.user_addr[username] = address
        self.addr_user[address] = username
        raise gen.Return(data)
