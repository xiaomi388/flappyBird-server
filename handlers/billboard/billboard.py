# coding:utf-8
from tornado import gen
from handlers.base import BaseHandler


class BillboardHandler(BaseHandler):
    @gen.coroutine
    def handle(self, recv_data, address):
        data = {'error': ''}
        if address not in self.addr_user:
            data['error'] = u'您还未登陆，请先登录'
            raise gen.Return(data)
        cur_user = self.addr_user[address]
        mode = recv_data['mode']
        count = recv_data['count']
        offset = recv_data['offset']
        sql = '''SELECT username, MAX(score) FROM record WHERE mode = ?
                GROUP BY username ORDER BY MAX(score) desc limit ?,?'''
        c = self.CONN.cursor()
        c.execute(sql, (mode, offset, count))
        data['content'] = []
        for index, rec in enumerate(c):
            res = {}
            res['rank'] = index + offset + 1
            res['username'] = rec[0]
            res['max_score'] = rec[1]
            data['content'].append(res)
        sql = '''SELECT t1.username, t1.score FROM
                (SELECT username, MAX(score) AS score FROM record
                WHERE mode = ? AND username = ?
                GROUP BY username ORDER BY MAX(score) desc) AS t1'''
        c.execute(sql, (mode, cur_user))
        res = {}
        recs = c.fetchall()
        if len(recs) == 0:
            res['rank'] = 0
            res['username'] = cur_user
            res['max_score'] = 0
            data['content'].append(res)
            raise gen.Return(data)
        res['username'] = recs[0][0]
        res['max_score'] = recs[0][1]
        sql = '''SELECT COUNT(*) AS rank FROM
                (SELECT MAX(score) AS rank FROM record
                WHERE mode = ?  GROUP BY username
                HAVING MAX(score) > ?) AS t2'''
        c.execute(sql, (mode, res['max_score']))
        res['rank'] = c.fetchall()[0][0] + 1
        data['content'].append(res)
        raise gen.Return(data)
