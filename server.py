# coding:utf-8
import sqlite3

from tornado.tcpserver import TCPServer
from tornado.iostream import StreamClosedError
from tornado import gen
from tornado.ioloop import IOLoop
from utility import pack, unpack

from router import routers
from manager import manager


def initial():
    CONN = sqlite3.connect('flappy.db')
    c = CONN.cursor()
    query = c.execute('''select * from sqlite_master''')
    if len(query.fetchall()) != 0:
        return
    c.execute('''CREATE TABLE user (username VARCHAR,
        password VARCHAR)''')
    c.execute(''' CREATE TABLE record
            (username VARCAHR, game_id VARCHAR,
            mode VARCHAR, ttl INTEGER, score INTEGER,
            pipe_num INTEGER)''')
    print 'init success'
    CONN.commit()
    CONN.close()


class TcpConnection(object):
    def __init__(self, stream, address):
        self._stream = stream
        self._address = address

    @gen.coroutine
    def handle(self):
        while True:
            try:
                length = yield self.read_message(3)
                length = int(length)
                recv_data = yield self.read_message(length)
                recv_data = unpack(recv_data)
                print recv_data
                send_data = {
                    'error': u'无法识别包内容，请检查method参数是否正确！',
                }
                # 消息处理
                method = None
                if 'method' in recv_data:
                    method = recv_data['method']
                if method in routers:
                    handler = routers[method]()
                    # 检查数据包是否过期
                    if not handler.check_timestamp(recv_data):
                        send_data['error'] = u'该数据包已过期'
                    else:
                        # 正常处理
                        send_data = yield handler.handle(
                            recv_data, self._address)
                    send_data['method'] = method
                yield self.send_message(pack(send_data))
            except StreamClosedError:
                print 'bye {}'.format(self._address)
                handler = routers['clean']()
                yield handler.handle(None, self._address)
                raise gen.Return(None)

    @gen.coroutine
    def read_message(self, length):
        raise gen.Return((yield self._stream.read_bytes(length)))

    @gen.coroutine
    def send_message(self, data):
        raise gen.Return((yield self._stream.write(data)))


class GameServer(TCPServer):
    ''' 服务器主入口，负责处理流的读写 '''

    @gen.coroutine
    def handle_stream(self, stream, address):
        print 'new connection from {}'.format(address)
        conn = TcpConnection(stream, address)
        manager.add(TcpConnection(stream, address), address)
        yield conn.handle()
        manager.remove(address)


if __name__ == "__main__":
    initial()
    SERVER = GameServer()
    SERVER.listen(9234)
    IOLoop.current().start()
