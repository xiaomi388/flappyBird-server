#!/usr/local/bin/python
# -*- coding: utf-8 -*-
from binascii import b2a_hex, a2b_hex
import socket
import json
import base64
import hashlib
import platform

if platform.system().find("windows") == -1:
    from Crypto.Cipher import AES
else:
    from crypto.Cipher import AES
# constants
TIMEOUT = -1
CLOSED = -2
EMPTY = -3  # means read empty data

# 功能：对输入的dict使用json转换 使用base64加密 加上长度信息
# 输入：dict
# 输出：string


def pack(dic):
    # 转换成json
    jsonData = json.dumps(dic)
    # base64加密
    jsonData = base64.b64encode(jsonData)
    # 加上头部信息
    length = len(jsonData)
    string = None
    if length < 10 and length > 0:
        string = "00" + str(length) + jsonData
    elif length < 100:
        string = "0" + str(length) + jsonData
    elif length < 1000:
        string = str(length) + jsonData
    else:
        string = "000"
    return string


# 功能：对输入的string（不含长度信息） 进行base64解密 再用json转换 得到dict
# 输入string
# 输出dict


def unpack(string):
    # base64解密
    jsonData = base64.b64decode(string)
    # json解析
    dic = json.loads(jsonData)
    return dic


def to_sha(string):
    h = hashlib.sha1(string)
    return h.hexdigest()


class prpcrypt():
    def __init__(self, key):
        self.key = key
        self.mode = AES.MODE_CBC

    def encrypt(self, text):
        cryptor = AES.new(self.key, self.mode, self.key)
        length = 16
        count = len(text)
        if (count % length != 0):
            add = length - (count % length)
        else:
            add = 0
        text = text + ('\0' * add)
        self.ciphertext = cryptor.encrypt(text)
        return b2a_hex(self.ciphertext)

    # 解密后，去掉补足的空格用strip() 去掉
    def decrypt(self, text):
        cryptor = AES.new(self.key, self.mode, self.key)
        plain_text = cryptor.decrypt(a2b_hex(text))
        return plain_text.rstrip('\0')
