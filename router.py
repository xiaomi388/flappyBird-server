# coding:utf-8

#from handlers.billboard.billboard import BillboardHandler
from handlers.user.login import LoginHandler
from handlers.user.logout import LogoutHandler
from handlers.user.register import RegisterHandler
from handlers.singleGame.newGame import NewGameHandler
from handlers.singleGame.endGame import EndGameHandler
from handlers.singleGame.syncGame import SyncGameHandler
from handlers.multiGame.newGame import NewGameHandler as MultiNewGameHandler
from handlers.multiGame.exitGame import ExitGameHandler as MultiExitGameHandler
from handlers.multiGame.event import EventHandler as MultiGameEventHandler
from handlers.cleanHandler import CleanHandler
from handlers.billboard.billboard import BillboardHandler

routers = {
    'login': LoginHandler,
    'logout': LogoutHandler,
    'register': RegisterHandler,
    'clean': CleanHandler,
    'new-single-game': NewGameHandler,
    'end-single-game': EndGameHandler,
    'sync-single-game': SyncGameHandler,
    'billboard': BillboardHandler,
    'new-multi-game': MultiNewGameHandler,
    'exit-multi-game': MultiExitGameHandler,
    'send-event-multi-game': MultiGameEventHandler,
    'get-best-score': BillboardHandler
}
